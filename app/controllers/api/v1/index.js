/**
 * @file contains entry point of controllers api v1 module
 * @author Fikri Rahmat Nurhidayat
 */

const user = require("./user");
const book = require("./book");
const orderbook = require("./order_book");
const payment = require("./payment");

module.exports = {
  user,
  book,
  orderbook,
  payment,
};
