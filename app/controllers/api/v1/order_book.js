/**
 * @file contains request handler of post resource
 * @author Fikri Rahmat Nurhidayat
 */
const { order_book, book, user } = require("../../../models");
const orderBookServices = require("../../../services/order_book");

module.exports = {
  list(req, res) {
    orderBookServices
      .getAll({
        attributes: { exclude: ["createdAt", "updatedAt"] },
        include: [
          {
            model: book,
            attributes: ["name", "price", "publisher"],
          },
          {
            model: user,
            attributes: ["username", "email"],
          },
        ],
      })
      .then((order_book) => {
        res.status(200).json({
          status: "OK",
          data: {
            order_book,
          },
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  create(req, res) {
    const { id_user, id_book, total_price, amount } = req.body;
    orderBookServices
      .create(id_user, id_book, total_price, amount)
      .then((order_book) => {
        res.status(201).json({
          status: "OK",
          data: order_book,
        });
      })
      .catch((err) => {
        res.status(201).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  update(req, res) {
    orderBookServices
      .update(req.order, req.body)
      .then((order) => {
        res.status(200).json({
          status: "OK",
          data: order,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  show(req, res) {
    const order = req.order;

    res.status(200).json({
      status: "OK",
      data: order,
    });
  },

  destroy(req, res) {
    orderBookServices
      .delete(req.order)
      .then(() => {
        res.status(204).end();
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  setOrder(req, res, next) {
    orderBookServices
      .choose(req.params.id)
      .then((order) => {
        if (!order) {
          res.status(404).json({
            status: "FAIL",
            message: "Order not found!",
          });

          return;
        }

        req.order = order;
        next();
      })
      .catch((err) => {
        res.status(404).json({
          status: "FAIL",
          message: "Order not found!",
        });
      });
  },
};
