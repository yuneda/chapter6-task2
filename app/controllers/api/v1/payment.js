/**
 * @file contains request handler of post resource
 * @author Fikri Rahmat Nurhidayat
 */
const { user, order_book } = require("../../../models");
const paymentServices = require("../../../services/payment");

module.exports = {
  list(req, res) {
    paymentServices
      .getAll({
        include: {
          model: order_book,
        },
      })
      .then((payment) => {
        res.status(200).json({
          status: "OK",
          data: {
            payment,
          },
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  create(req, res) {
    const { id_order, payment_method } = req.body;
    paymentServices
      .create(id_order, payment_method)
      .then((payment) => {
        res.status(201).json({
          status: "OK",
          data: payment,
        });
      })
      .catch((err) => {
        res.status(201).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  update(req, res) {
    paymentServices
      .update(req.payment, req.body)
      .then((payment) => {
        res.status(200).json({
          status: "OK",
          data: payment,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  show(req, res) {
    paymentServices.choose(req.params.id).then((payment) => {
      res.status(200).json({
        status: "OK",
        data: payment,
      });
    });
  },

  destroy(req, res) {
    paymentServices
      .delete(req.payment)
      .then(() => {
        res.status(204).end();
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  setpayment(req, res, next) {
    paymentServices
      .choose(req.params.id)
      .then((payment) => {
        if (!payment) {
          res.status(404).json({
            status: "FAIL",
            message: "Post not found!",
          });

          return;
        }

        req.payment = payment;
        next();
      })
      .catch((err) => {
        res.status(404).json({
          status: "FAIL",
          message: "Post not found!",
        });
      });
  },
};
