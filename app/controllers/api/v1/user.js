/**
 * @file contains request handler of post resource
 * @author Fikri Rahmat Nurhidayat
 */
const { user, order_book } = require("../../../models");
const userServices = require("../../../services/user");

module.exports = {
  list(req, res) {
    userServices
      .getAll({
        include: {
          model: order_book,
        },
      })
      .then((users) => {
        res.status(200).json({
          status: "OK",
          data: {
            users,
          },
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  create(req, res) {
    const { username, email, password } = req.body;
    userServices
      .create(username, email, password)
      .then((user) => {
        res.status(201).json({
          status: "OK",
          data: user,
        });
      })
      .catch((err) => {
        res.status(201).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  update(req, res) {
    userServices
      .update(req.user, req.body)
      .then((user) => {
        res.status(200).json({
          status: "OK",
          data: user,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  show(req, res) {
    userServices
      .choose(req.params.id)
      .then((user) => {
        res.status(200).json({
          status: "OK",
          data: user,
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  destroy(req, res) {
    userServices
      .delete(req.user)
      .then(() => {
        res.status(204).end();
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  setUser(req, res, next) {
    userServices
      .choose(req.params.id)
      .then((user) => {
        if (!user) {
          res.status(404).json({
            status: "FAIL",
            message: "Post not found!",
          });

          return;
        }

        req.user = user;
        next();
      })
      .catch((err) => {
        res.status(404).json({
          status: "FAIL",
          message: "Post not found!",
        });
      });
  },
};
