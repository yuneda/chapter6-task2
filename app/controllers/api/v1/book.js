/**
 * @file contains request handler of post resource
 * @author Fikri Rahmat Nurhidayat
 */
const { book, order_book } = require("../../../models");
const bookServices = require("../../../services/book");

module.exports = {
  list(req, res) {
    bookServices
      .getAll({
        include: {
          model: order_book,
        },
      })
      .then((books) => {
        res.status(200).json({
          status: "OK",
          data: {
            books,
          },
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  create(req, res) {
    const { name, price, publisher } = req.body;
    bookServices
      .create(name, price, publisher)
      .then((book) => {
        res.status(201).json({
          status: "OK",
          data: book,
        });
      })
      .catch((err) => {
        res.status(201).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  update(req, res) {
    bookServices
      .update(req.book, req.body)
      .then((book) => {
        res.status(200).json({
          status: "OK",
          data: book,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  show(req, res) {
    const book = req.book;

    res.status(200).json({
      status: "OK",
      data: book,
    });
  },

  destroy(req, res) {
    bookServices
      .delete(req.book)
      .then(() => {
        res.status(204).end();
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  setBook(req, res, next) {
    // book.findByPk(req.params.id);
    bookServices
      .choose(req.params.id)
      .then((book) => {
        if (!book) {
          res.status(404).json({
            status: "FAIL",
            message: "Book not found!",
          });

          return;
        }

        req.book = book;
        next();
      })
      .catch((err) => {
        res.status(404).json({
          status: "FAIL",
          message: "Book not found!",
        });
      });
  },
};
