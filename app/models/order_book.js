"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class order_book extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      order_book.belongsTo(models.user, {
        foreignKey: "id_user",
      });
      order_book.belongsTo(models.book, {
        foreignKey: "id_book",
      });
      order_book.hasOne(models.payment, {
        foreignKey: "id_order",
      });
    }
  }
  order_book.init(
    {
      id_user: DataTypes.INTEGER,
      id_book: DataTypes.INTEGER,
      total_price: DataTypes.INTEGER,
      amount: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "order_book",
    }
  );
  return order_book;
};
