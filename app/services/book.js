const { book } = require("../models");

module.exports = {
  getAll(a) {
    return book.findAll(a);
  },
  create(name, price, publisher) {
    return book.create({ name, price, publisher });
  },
  choose(id) {
    return book.findByPk(id);
  },
  delete(oldUser) {
    return oldUser.destroy();
  },
  update(oldUser, requestBody) {
    return oldUser.update(requestBody);
  },
};
