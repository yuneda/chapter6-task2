const { user } = require("../models");

module.exports = {
  getAll(a) {
    return user.findAll(a);
  },
  create(username, email, password) {
    return user.create({ username, email, password });
  },
  choose(id) {
    return user.findByPk(id);
  },
  delete(oldUser) {
    return oldUser.destroy();
  },
  update(oldUser, requestBody) {
    return oldUser.update(requestBody);
  },
};
