const { payment } = require("../models");

module.exports = {
  getAll(a) {
    return payment.findAll(a);
  },
  create(id_order, payment_method) {
    return payment.create({ id_order, payment_method });
  },
  choose(id) {
    return payment.findByPk(id);
  },
  delete(oldUser) {
    return oldUser.destroy();
  },
  update(oldUser, requestBody) {
    return oldUser.update(requestBody);
  },
};
