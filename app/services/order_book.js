const { order_book } = require("../models");

module.exports = {
  getAll(a) {
    return order_book.findAll(a);
  },
  create(id_user, id_book, total_price, amount) {
    return order_book.create({ id_user, id_book, total_price, amount });
  },
  choose(id) {
    return order_book.findByPk(id);
  },
  delete(oldUser) {
    return oldUser.destroy();
  },
  update(oldUser, requestBody) {
    return oldUser.update(requestBody);
  },
};
