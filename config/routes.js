const express = require("express");
const controllers = require("../app/controllers");

const appRouter = express.Router();
const apiRouter = express.Router();

/** Mount GET / handler */
appRouter.get("/", controllers.main.index);

/**
 * TODO: Implement your own API
 *       implementations
 */
// apiRouter.get("/api/v1/posts", controllers.api.v1.post.list);
// apiRouter.post("/api/v1/posts", controllers.api.v1.post.create);
// apiRouter.put("/api/v1/posts/:id", controllers.api.v1.post.setPost, controllers.api.v1.post.update);
// apiRouter.get("/api/v1/posts/:id", controllers.api.v1.post.setPost, controllers.api.v1.post.show);
// apiRouter.delete("/api/v1/posts/:id", controllers.api.v1.post.setPost, controllers.api.v1.post.destroy);

apiRouter.get("/api/v1/users", controllers.api.v1.user.list);
apiRouter.post("/api/v1/users", controllers.api.v1.user.create);
apiRouter.put("/api/v1/users/:id", controllers.api.v1.user.setUser, controllers.api.v1.user.update);
apiRouter.get("/api/v1/users/:id", controllers.api.v1.user.show);
apiRouter.delete("/api/v1/users/:id", controllers.api.v1.user.setUser, controllers.api.v1.user.destroy);

apiRouter.get("/api/v1/books", controllers.api.v1.book.list);
apiRouter.post("/api/v1/books", controllers.api.v1.book.create);
apiRouter.put("/api/v1/books/:id", controllers.api.v1.book.setBook, controllers.api.v1.book.update);
apiRouter.get("/api/v1/books/:id", controllers.api.v1.book.setBook, controllers.api.v1.book.show);
apiRouter.delete("/api/v1/books/:id", controllers.api.v1.book.setBook, controllers.api.v1.book.destroy);

apiRouter.get("/api/v1/orderbook", controllers.api.v1.orderbook.list);
apiRouter.post("/api/v1/orderbook", controllers.api.v1.orderbook.create);
apiRouter.put("/api/v1/orderbook/:id", controllers.api.v1.orderbook.setOrder, controllers.api.v1.orderbook.update);
apiRouter.get("/api/v1/orderbook/:id", controllers.api.v1.orderbook.setOrder, controllers.api.v1.orderbook.show);
apiRouter.delete("/api/v1/orderbook/:id", controllers.api.v1.orderbook.setOrder, controllers.api.v1.orderbook.destroy);

apiRouter.get("/api/v1/payments", controllers.api.v1.payment.list);
apiRouter.post("/api/v1/payments", controllers.api.v1.payment.create);
apiRouter.put("/api/v1/payments/:id", controllers.api.v1.payment.setpayment, controllers.api.v1.payment.update);
apiRouter.get("/api/v1/payments/:id", controllers.api.v1.payment.setpayment, controllers.api.v1.payment.show);
apiRouter.delete("/api/v1/payments/:id", controllers.api.v1.payment.setpayment, controllers.api.v1.payment.destroy);
/**
 * TODO: Delete this, this is just a demonstration of
 *       error handler
 */
apiRouter.get("/api/v1/errors", () => {
  throw new Error("The Industrial Revolution and its consequences have been a disaster for the human race.");
});

apiRouter.use(controllers.api.main.onLost);
apiRouter.use(controllers.api.main.onError);

/**
 * TODO: Delete this, this is just a demonstration of
 *       error handler
 */
appRouter.get("/errors", () => {
  throw new Error("The Industrial Revolution and its consequences have been a disaster for the human race.");
});

appRouter.use(apiRouter);

/** Mount Not Found Handler */
appRouter.use(controllers.main.onLost);

/** Mount Exception Handler */
appRouter.use(controllers.main.onError);

module.exports = appRouter;
