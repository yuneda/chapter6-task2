"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "payments",
      [
        {
          id: 1,
          id_order: 1,
          payment_method: "Indomaret",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 2,
          id_order: 2,
          payment_method: "Cash",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
