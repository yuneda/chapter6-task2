"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "order_books",
      [
        {
          id: 1,
          id_user: 1,
          id_book: 1,
          total_price: 70000,
          amount: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 2,
          id_user: 1,
          id_book: 2,
          total_price: 85000,
          amount: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 3,
          id_user: 2,
          id_book: 1,
          total_price: 70000,
          amount: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 4,
          id_user: 3,
          id_book: 3,
          total_price: 90000,
          amount: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
