"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "users",
      [
        {
          id: 1,
          username: "Anya Geraldine",
          email: "anya@gmail.com",
          password: "anya123",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 2,
          username: "Isyana Sarasvati",
          email: "isyana@gmail.com",
          password: "isyana123",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 3,
          username: "Raisa",
          email: "raisa@gmail.com",
          password: "Raisa123",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 4,
          username: "Sri Wahyuni",
          email: "sri@gmail.com",
          password: "sri123",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
