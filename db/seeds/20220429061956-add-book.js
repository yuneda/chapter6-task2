"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "books",
      [
        {
          id: 1,
          name: "Guru Aini",
          price: 70000,
          publisher: "Mizan Pustaka",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 2,
          name: "Kamu Terlalu Banyak Bercanda",
          price: 85000,
          publisher: "Media Komputindo",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 3,
          name: "Dear Tomorrow: Notes to My Future Self",
          price: 90000,
          publisher: "Gagas Media",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 4,
          name: "Nanti Kita Cerita Tentang Hari ini",
          price: 95000,
          publisher: "Bintang Medis",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
